package edu.uoc.daada_pac3_notifications.model

import android.arch.persistence.room.*
import android.content.Context

@Database(entities = [BookItem::class], version = 1)
abstract class BookContent: RoomDatabase(){
    abstract fun bookDataAccesObject(): BookDao

    companion object {
        var INSTANCE: BookContent? = null

        fun getAppDataBase(context: Context): BookContent? {
            if (INSTANCE == null){
                synchronized(BookContent::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, BookContent::class.java, "BooksDatabase").allowMainThreadQueries().build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}

@Dao
interface BookDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addBook(book: BookItem): Long

    @Update()
    fun updateBook(book: BookItem)

    @Delete()
    fun deleteBook(book: BookItem)

    @Query("SELECT * FROM book_table WHERE title = :bookTitle LIMIT 1")
    fun findBookByTitle(bookTitle: String): BookItem?

    @Query("SELECT * FROM book_table WHERE identifier = :bookIdentifier LIMIT 1")
    fun getBook(bookIdentifier: Int): BookItem?

    @Query("SELECT * FROM book_table ORDER BY identifier ASC")
    fun getAllBooks(): List<BookItem>

    @Query("DELETE FROM book_table")
    fun deleteAllBooks()

    @Query("SELECT * FROM book_table WHERE title LIKE :bookTitle AND author LIKE :bookAuthor AND publication_date LIKE :bookPublicationDate")
    fun getDuplicates(bookTitle: String, bookAuthor: String, bookPublicationDate: String): List<BookItem>?
}

/*class DateTypeConverter {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun toTimestamp(date: Date?): Long? {
        return date?.time
    }
}*/
