package edu.uoc.daada_pac3_notifications

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import edu.uoc.daada_pac3_notifications.model.BookContent
import edu.uoc.daada_pac3_notifications.model.BookDao
import edu.uoc.daada_pac3_notifications.model.BookItem
import kotlinx.android.synthetic.main.activity_book_detail.*
import android.widget.TextView

class BookDetailActivity : AppCompatActivity() {

    private lateinit var book: BookItem
    private lateinit var roomDatabase: BookContent
    private lateinit var bookDAO: BookDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_detail)

        roomDatabase = BookContent.getAppDataBase(this.applicationContext)!!
        bookDAO = roomDatabase.bookDataAccesObject()

        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        // Add an upper arrow to return to activity BookListActivity (defined in the manifest)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val bookIdentifier: Int = intent.getIntExtra("BOOK_ITEM_IDENTIFIER", 0)


        book = bookDAO.getBook(bookIdentifier) ?: bookDAO.getAllBooks().first()

        var authorTextView = findViewById(R.id.author) as TextView
        authorTextView.text = book.author
        var publicationDateTextView = findViewById(R.id.publicationDate) as TextView
        publicationDateTextView.text = book.publicationDate
        var descriptionTextView = findViewById(R.id.description) as TextView
        descriptionTextView.text = book.description
    }
}
